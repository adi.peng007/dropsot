<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/lv2.css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  
</head>
<body>
<div class="menu">
    <ul>
    <div class="logo"> <a href="indexlv2.php"> <img src="../foto/log.png" /></a></div>
    <li><a href="../logout.php">LOGOUT   <i class="fa fa-sign-out "> </i></a></li>      
	</ul>
	<div class="menu2">
    <ul>
		<li><a href="marketplace.php">Marketplace </i></a></li>   
		<li><a href="#">Data Transaksi </i></a></li>
		<li><a href="#">Dompetku </i></a></li>
		<li><a href="#">Gudang Informasi </i></a></li>   
		<li><a href="#">Profilku </i></a></li>
		<li><a href="#">Panduan </i></a></li>
	</ul>
	</div>
</div>
<center>
	<div class="malasngoding-slider pt-lg-5">
		<div class="isi-slider">
			<img src="../foto/1.png" alt="Gambar 1">

		</div>
	</div>
	<div class=menu-user>
		<div class="isi-menu ">
			<table>
				<tr>
				<td><form href="marketplace.php"><button id="WA"><i class="fa fa-shopping-cart fa-2x"></i><br> Marketplace</button></form></td>
				<td><form href="indexlv2.php"><button id="WA"><i class="fa fa-file-text fa-2x"></i><br>Data Transaksi</button></form></td>
				<td><form href="indexlv2.php"><button id="WA"><i class="fa fa-money fa-2x"></i><br>Dompetku</button></form></td>
				<tr>
				<td><form href="indexlv2.php"><button id="WA"><i class="fa fa-lightbulb-o fa-2x"></i><br>Gudang Informasi</button></form></td>
				<td><form href="indexlv2.php"><button id="WA"><i class="fa fa-user fa-2x"></i><br>Profilku</button></form></td>
				<td><form href="indexlv2.php"><button id="WA"><i class="fa fa-info-circle fa-2x"></i><br>Panduan</button></form></td>
				</tr>
			</table>
	</div>
</div>
</center>

<div class="container my-md-5 pb-lg-5">
	<div class="d-flex justify-content-center pb-lg-5"><p class="h1">Produk Knalpot</p></div>
	<div class="row products">
			
	</div>
</div>
	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script>
	function getAllProduct(){
		let products = ''
		$.ajax({
			url: `../api/api_produk.php?action=get_all_product`,
			type: "GET",
			dataType: "json",
			success: function(response){
				$("card:first").hide()
				$.each(response.data, function(idx, value){
					products =  `
					<div class="col-lg-3 pb-lg-3">
						<div class="card">
							<img src="${value.foto_produk}" class="card-img-top" alt="${value.nama_produk}">
							<div class="card-body">
								<h5 class="card-title">${value.nama_produk}</h5>
								<h4 class="card-title">Rp. ${value.harga_jual}</h4>
								<p class="card-text">${value.deskripsi}</p>
							</div>
							<div class="card-footer">
								<button type="button" class="btn btn-primary">Beli</button>
							</div>
						</div>
					</div>
					`
					$(".products").append(products)
				})

			},
			error: function(jqXHR, textStatus, erro){
				console.log(jqXHR)
			}

		})
	}
	
	getAllProduct()
  </script>
</body>
</html>