<!DOCTYPE html>
<html>
<head>
  <title>Selamat Datang</title>
  <!-- menghubungkan dengan file css -->
  <link rel="stylesheet" type="text/css" href="welcome.css">
  <!-- menghubungkan dengan file jquery -->
  <script type="text/javascript" src="jquery.js"></script>
  <div class="menu">
    <ul>
     <div class="logo"> <img src="foto/logo.png"/></div>
         <li><a href="logout.php">Logout</a></li>
         <li><a>|</a></li>
         <li><a href="welcome.php?page=evaluasi">Evaluasi</a></li>
       <li><a href="welcome.php?page=materi">Materi</a></li>
       <li><a href="welcome.php?page=program">Jadwal</a></li>
       <li><a href="welcome.php?page=beranda">Beranda</a></li>
    </ul>
  </div>
</head>
<body>

<div class="content">
  <div class="badan">
  <?php 
  session_start();

  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['level']==""){
    header("location:login.php?pesan=gagal");
  }

  ?>
 
  <?php 
  if(isset($_GET['page'])){
    $page = $_GET['page'];
 
    switch ($page) {
      case 'beranda':
        include "halaman/beranda.php";
        break;
      case 'program':
        include "halaman/program.php";
        break;
              case 'materi':
        include "halaman/materi.php";
        break;
        case 'evaluasi':
        include "halaman/evaluasi.php";
        break;      
      default:
        echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
        break;
    }
  }else{
    include "halaman/beranda.php";
  }
 
   ?>
 
  </div>
</body>
</html>