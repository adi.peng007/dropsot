<head>
<link rel="stylesheet" type="text/css" href="../css/lv1.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
</head>
<body>
<script src="../css/js/jquery.min.js"></script>
<script src="../css/js/bootstrap.min.js"></script>
<script src="../css/js/sweetalert2.min.js"></script>

<?php
session_start(); 
if (!isset($_SESSION["username"])) {
	header("location: ../index.php?page=login");
	exit;
}
$id_ds=isset($_SESSION['id_ds']) ? $_SESSION['id_ds'] : die('ERROR: Record ID not found.');
//include database connection
include '../koneksi.php';
 
// read current record's data
try {
    // prepare select query
   $query = "SELECT id_ds, no_rek, nama_rek, bank, foto_ktp FROM dropshipper WHERE id_ds = ? LIMIT 0,1";
    $stmt = $con->prepare( $query );
     
    // this is the first question mark
    $stmt->bindParam(1, $id_ds);
     
    // execute our query
    $stmt->execute();
     
    // store retrieved row to a variable
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
    // values to fill up our form
    
    $no_rek = $row['no_rek'];
    $nama_rek = $row['nama_rek'];
    $bank = $row['bank'];
    $foto_ktp = $row['foto_ktp'];
}
 
// show error
catch(PDOException $exception){
    die('ERROR: ' . $exception->getMessage());
}
?>
        <?php
 
// check if form was submitted
if($_POST){
         include '../koneksi.php';
    try{
     
        // write update query
        // in this case, it seemed like we have so many fields to pass and 
        // it is better to label them and not use question marks
        $query = "UPDATE dropshipper 
                    SET no_rek=:no_rek, nama_rek=:nama_rek, bank=:bank , foto_ktp=:foto_ktp
                    WHERE id_ds=:id_ds";
 
        // prepare query for excecution
        $stmt = $con->prepare($query);
 
        // posted values
        $no_rek=$_POST['no_rek'];
        $nama_rek=$_POST['nama_rek'];
        $bank=$_POST['bank'];
        $foto_ktp=!empty($_FILES["foto_ktp"]["name"])
        ? basename($_FILES["foto_ktp"]["name"])
        : "";
        $foto_ktp=$foto_ktp;
  
 
        // bind the parameters
                $stmt->bindParam(':id_ds', $id_ds);
        $stmt->bindParam(':no_rek', $no_rek);
        $stmt->bindParam(':nama_rek', $nama_rek);
        $stmt->bindParam(':bank', $bank);
        $stmt->bindParam(':foto_ktp', $foto_ktp);

         
        // Execute the query
        if($stmt->execute()){
          header("refresh:3;url=indexlv1.php?page=lengkap");
          echo "<script>Swal.fire({type: 'success', title: 'Upload Berhasil'});</script>";
        
if($foto_ktp){
 
    // sha1_file() function is used to make a unique file name
    $target_directory = "../foto/ktp/";
    $target_file = $target_directory . $foto_ktp;
    $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
 
    // error message is empty
    $file_upload_error_messages="";
    $allowed_file_types=array("jpg", "jpeg", "[png]");
    if(!in_array($file_type, $allowed_file_types)){
        $file_upload_error_messages.="<div>Hanya file PPT, PPTX, PDF yang diperbolehkan</div>";
    }
if(!is_dir($target_directory)){
    mkdir($target_directory, 0777, true);
}
// if $file_upload_error_messages is still empty
if(empty($file_upload_error_messages)){
    // it means there are no errors, so try to upload the file
    if(move_uploaded_file($_FILES["foto_ktp"]["tmp_name"], $target_file)){
    }
  }
    }else{
      
      header ("indexlv1.php?page=lengkapi","pesan=gagal");   
    }
          }
        }
     
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>
</body>