<?php 
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    if(function_exists($_GET['action'])) {
         $_GET['action']();
    }   

    function create_transaksi(){

    }

    function get_all_transaksi(){
        global $connect;
        $data = array();

        $query = mysqli_query($connect, "SELECT * FROM transaksi");

        while($row = mysqli_fetch_object($query))
        {
            $data[] =$row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '.mysqli_error($query));
        }


        header('Content-Type: application/json');
        echo json_encode($response);

    }


    function get_transaksi_by_id(){
        {
        global $connect;
        $id = $_GET['id'];
        $data = [];
        $query = mysqli_query($connect,"SELECT * FROM transaksi WHERE id_transaksi=". $id);
      
        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }
        
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    }

    function get_transaksi_by_status(){
        global $connect;
        $status_transaksi = $_GET['status'];
        $data = array();
        $query = mysqli_query($connect, "SELECT * FROM transaksi WHERE status_transaksi =". $status_transaksi);

        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }

        if ($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data,
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function ubah_status_transaksi(){
        global $connect;
        $id = $_POST['id'];
        $status_transaksi = $_POST['status_transaksi'];
        $query = mysqli_query($connect, "UPDATE transaksi SET
        status_transaksi = '".$status_transaksi."',
        WHERE id_transaksi=".$id);

        if($query){
            $response = array(
                'status' => 200,
                'message' => "Success",
            );
        }else{
            die('Error: '.mysqli_error($query));
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function delete_transaksi(){
        global $connect;
        $id = $_GET['id'];
        $query = mysqli_query($connect,"DELETE FROM transaksi WHERE id_transaksi=". $id);
        if ($query){
            $response = array(
                'status' => 200,
                'message' => "data berhasil dihapus.",
            );
        }else{
            die('Error: '. mysqli_error($query));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>