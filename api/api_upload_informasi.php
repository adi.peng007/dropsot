<?php
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once __ROOT__.'/database/koneksi.php';

    function get_all_informasi(){
        global $connect;
        $data = array();

        $query = mysqli_query($connect, "SELECT * FROM gudang_informasi");

        while($row = mysqli_fetch_object($query))
        {
            $data[] = $row;
        }

        if($query){
            $response = array(
                'status' => 200,
                'message' => "success",
                'data' => $data
            );
        }else{
            die('Error: '. mysqli_error($query));

        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function upload_informasi(){
        $target_folder = '../upload';
        $nama_informasi = $_POST['nama_informasi'];
        $deskripsi = $_POST['deskripsi'];
        $file_name = $target_folder . basename($_FILES['file']['name']);
        $file_type = $_FILES['file']['type'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path = $protocol . "://" . $_SERVER['SERVER_NAME']."/dropshot"."/upload/".$file_name;

        if ($file_type=="application/pdf") {
            if(move_uploaded_file($_FILES['file']['tmp_name'], $targetfolder))
            {
                $query = mysqli_query($connect, "INSERT INTO gudang_informasi(
                    nama_inf,
                    deskripsi,
                    file_dok,
                )
                VALUES(
                    '$nama_informasi',
                    '$deskripsi',
                    '$path'"
                );

                if($result){
                    $response = array(
                        'status' => 200,
                        'message' => "success",
                    );
                }else{
                    die('Error: '. mysqli_error($result));
                }
            }
            else {
                echo "Problem uploading file";
            }
            header('Content-Type: application/json');
            echo json_encode($response);
        }
        else {
            echo "You may only upload PDFs, JPEGs or GIF files.<br>";
        }
    }
?>