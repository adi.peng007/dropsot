-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Jul 2022 pada 03.13
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropsot`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dropshipper`
--

CREATE TABLE `dropshipper` (
  `id_ds` int(100) NOT NULL,
  `nama_ds` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(100) NOT NULL,
  `no_rek` int(100) NOT NULL,
  `nama_rek` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `foto_ktp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dropshipper`
--

INSERT INTO `dropshipper` (`id_ds`, `nama_ds`, `gender`, `alamat`, `no_hp`, `email`, `username`, `password`, `level`, `no_rek`, `nama_rek`, `bank`, `foto_ktp`) VALUES
(1, 'adi', 'laki-laki', '', '', '', 'admin', 'admin', 3, 0, '', '', ''),
(2, 'adipeng', 'Laki-Laki', 'tajug', '085879887675', 'adi.peng007@gmail.com', 'adi', 'adi123', 2, 42342342, 'adi', 'BRI', '2.png'),
(3, 'zaki', 'Laki-Laki', 'asadsad', '213124124', 'sadadad', 'zaki', 'zaki', 1, 2147483647, 'zaki', 'BRI', 'WhatsApp Image 2022-06-30 at 10.33.21 (1).jpeg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dropshipper`
--
ALTER TABLE `dropshipper`
  ADD PRIMARY KEY (`id_ds`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dropshipper`
--
ALTER TABLE `dropshipper`
  MODIFY `id_ds` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
