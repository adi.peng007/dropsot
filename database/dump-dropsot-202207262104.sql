-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: dropsot
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dropshipper`
--

DROP TABLE IF EXISTS `dropshipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dropshipper` (
  `id_ds` int(100) NOT NULL AUTO_INCREMENT,
  `nama_ds` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(100) NOT NULL,
  `no_rek` int(100) NOT NULL,
  `nama_rek` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `foto_ktp` varchar(100) NOT NULL,
  PRIMARY KEY (`id_ds`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dropshipper`
--

LOCK TABLES `dropshipper` WRITE;
/*!40000 ALTER TABLE `dropshipper` DISABLE KEYS */;
INSERT INTO `dropshipper` VALUES (2,'adipeng','Laki-Laki','tajug','085879887675','adi.peng007@gmail.com','adi','adi123',2,42342342,'adi','BRI','2.png'),(3,'zaki','Laki-Laki','asadsad','213124124','sadadad','zaki','zaki',1,1234567,'test','BRI','a3MRRBKN_700w_0.jpg'),(5,'admin','Laki-Laki','Jl. Admin no. 1','0857444888999','admin@gmail.com','admin','admin',3,0,'admin','BRI','');
/*!40000 ALTER TABLE `dropshipper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gudang_informasi`
--

DROP TABLE IF EXISTS `gudang_informasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gudang_informasi` (
  `id_inf` int(11) NOT NULL,
  `nama_inf` text NOT NULL,
  `file_dok` text NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gudang_informasi`
--

LOCK TABLES `gudang_informasi` WRITE;
/*!40000 ALTER TABLE `gudang_informasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `gudang_informasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `foto_produk` text DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES (52,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000),(53,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000),(54,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000),(55,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000),(56,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000),(57,'beras','beras asli jepang',20000,'http://localhost/dropshot/foto/upload/FOVXVjlXsAosFLk.jpg',30000);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `status_transaksi` text DEFAULT NULL,
  `nama_pembeli` text DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `no_hp` text DEFAULT NULL,
  `nama_pengirim` text DEFAULT NULL,
  `no_hp_pengirim` text DEFAULT NULL,
  `jasa_pengiriman` text DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi`
--

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_barang`
--

DROP TABLE IF EXISTS `transaksi_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_barang` (
  `id_barang` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `nama` text DEFAULT NULL,
  `berat` int(11) DEFAULT NULL,
  `jumlah` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_barang`),
  KEY `id_transaksi` (`id_transaksi`),
  CONSTRAINT `transaksi_barang_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_barang`
--

LOCK TABLES `transaksi_barang` WRITE;
/*!40000 ALTER TABLE `transaksi_barang` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dropsot'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-26 21:04:35
